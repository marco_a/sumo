local S = minetest.get_translator("sumo")

minetest.register_privilege("sumo_admin", {
    description = S("With this you can use /sumo create, edit")
})
